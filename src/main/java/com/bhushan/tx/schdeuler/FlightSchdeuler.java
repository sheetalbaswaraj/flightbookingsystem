package com.bhushan.tx.schdeuler;

import java.io.IOException;
import java.util.Timer;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.stereotype.Service;

import com.bhushan.tx.dto.FlightBookingAcknowledgement;

@Service
public class FlightSchdeuler {

	public static void sendMsgBeforeFlight(FlightBookingAcknowledgement flightBookingAcknowledgement)
			throws ClientProtocolException, IOException {
		Timer time = new Timer();
		FlightSchdeuleTask st = new FlightSchdeuleTask();

		time.schedule(st, 0, 10000);
		for (int i = 0; i <= 5; i++) {
			String authKey = "L1SypZi6xXz5tvgk4Ab7VIheqK9GjYcTC83BQNF0Ea2flnDPmO4TiKthmdeps5IZUC6NqxBP7F8MJcRV";
			String msg = "Hi_Folk_This_is_just_an_reminder_to_your_ticket_having_PNR" + "...."
					+ flightBookingAcknowledgement.getPnrNo() + "...." + "check_your_mail" + "...."
					+ flightBookingAcknowledgement.getPassengerInfo().getEmail() + "...." + "for_more_such_details" + ""
					+ flightBookingAcknowledgement.getStatus() + "Looking_forward_to_serve_you_again";
			HttpClient client = HttpClientBuilder.create().build();
			String url = "https://www.fast2sms.com/dev/bulk?authorization=" + authKey + "&sender_id=FSTSMS&message="
					+ msg + "&language=english&route=p&numbers="
					+ flightBookingAcknowledgement.getPassengerInfo().getMobileno();
			HttpUriRequest httpUriRequest = new HttpGet(url);
			client.execute(httpUriRequest);
			if (i == 5) {
				System.out.println("Application Terminates");
				client.execute(httpUriRequest);
				System.exit(0);
			}

		}

	}

}

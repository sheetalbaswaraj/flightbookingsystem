package com.bhushan.tx.service;

import java.io.IOException;
import java.time.LocalDate;
import java.util.UUID;

import javax.mail.MessagingException;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bhushan.tx.PdfGeneration.FlightAcknowledgementPdf;
import com.bhushan.tx.dto.FlightBookingAcknowledgement;
import com.bhushan.tx.dto.FlightBookingRequest;
import com.bhushan.tx.entity.PassengerInfo;
import com.bhushan.tx.entity.PaymentInfo;
import com.bhushan.tx.exception.InsufficientAmountException;
import com.bhushan.tx.repository.PassengerInfoRepository;
import com.bhushan.tx.repository.PaymentInfoRepository;
import com.bhushan.tx.schdeuler.FlightSchdeuler;
import com.bhushan.tx.sendmail.FlightAcknowledgementMail;
import com.bhushan.tx.sms.FlightStatusMsg;
import com.bhushan.tx.utils.PaymentUtils;

@Service
public class FlightBookingService {

	@Autowired
	private PassengerInfoRepository passengerInfoRepository;
	@Autowired
	private PaymentInfoRepository paymentInfoRepository;
	@Autowired
	private FlightAcknowledgementMail flightbookingmail;
	@Autowired
	private FlightSchdeuler flightschdeuler;

	@Autowired
	private FlightStatusMsg flightstatusmsg;

	@SuppressWarnings("static-access")
	@Transactional
	public FlightBookingAcknowledgement bookFlightTicket(FlightBookingRequest request)
			throws InsufficientAmountException, MailException, MessagingException, ClientProtocolException,
			IOException {
		
		 LocalDate today = LocalDate.now(); 
	     System.out.println("\nCurrent Date: "+today);
	     LocalDate plusDays = today.minusDays(-1);
	     
		PassengerInfo passengerInfo = request.getPassengerInfo();
		passengerInfo.setTraveldate(today);
		passengerInfo = passengerInfoRepository.save(passengerInfo);

		PaymentInfo paymentInfo = request.getPaymentInfo();

		PaymentUtils.validateCreditLimit(paymentInfo.getAccountno(), passengerInfo.getFare());

		paymentInfo.setPassengerid(passengerInfo.getPid());
		paymentInfo.setAmount(passengerInfo.getFare());
		paymentInfoRepository.save(paymentInfo);
		FlightBookingAcknowledgement flightBookingAcknowledgement = new FlightBookingAcknowledgement("SUCCESS",
				passengerInfo.getFare(), UUID.randomUUID().toString().split("-")[0], passengerInfo);
		FlightAcknowledgementPdf.createPdf(flightBookingAcknowledgement);
		flightbookingmail.sendEmail(flightBookingAcknowledgement);
		flightbookingmail.sendEmailWithAttachment(flightBookingAcknowledgement);
		flightstatusmsg.sendMsg(flightBookingAcknowledgement);
		System.out.println(plusDays);
		System.out.println(flightBookingAcknowledgement.getPassengerInfo().getTraveldate().compareTo(plusDays)<0);
		if(flightBookingAcknowledgement.getPassengerInfo().getTraveldate().compareTo(plusDays)<0) {
			flightschdeuler.sendMsgBeforeFlight(flightBookingAcknowledgement);
		}
		return flightBookingAcknowledgement;

	}
}
